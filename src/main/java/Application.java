import java.io.FileNotFoundException;

public class Application {

    public static void main(String arg[]) {
        System.out.println("Start");
        try {
            SamsungKnoxController samsungKnox = new SamsungKnoxController();
            samsungKnox.execute();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("End");
    }

}
