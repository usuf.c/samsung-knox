import com.samsung.knoxwsm.util.KnoxTokenUtility;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class SamsungKnoxController {

    public SamsungKnoxController() {
    }

    public void execute() throws FileNotFoundException {
        String myCertificateFilePath = "src/main/resources/certificate.json";
        String myClientIdentifier = "my.example.clientIdentifier";
        FileInputStream certInputStream = new FileInputStream(new File(myCertificateFilePath));

        // 1. generate signed clientIdentifier
        String signedClientIdentifier = KnoxTokenUtility.generateSignedClientIdentifierJWT(certInputStream, myClientIdentifier);

        // 2. call knox api to receive access token
        String apiAccessToken = getKnoxApiAccessToken(certInputStream, signedClientIdentifier);

        // 3. sign access token
        String signedApiAccessToken = KnoxTokenUtility.generateSignedAccessTokenJWT(certInputStream, apiAccessToken);

        // 4. call knox api to upload device
        // todo
    }

    private String getKnoxApiAccessToken(InputStream certInputStream, String signedClientIdentifier) throws FileNotFoundException {

        String apiAccessToken = "";

        // knox US endpoint to generate api access token
        String knoxGenerateApiAccessTokenEndpoint = "https://us-kcs-api.samsungknox.com/ams/v0/users/accesstoken";

        // create http request entity
        Map<String, String> map = new HashMap<String, String>();

        // set base64 encoded string public key
        map.put("base64EncodedStringPublicKey", KnoxTokenUtility.generateBase64EncodedStringPublicKey(certInputStream));

        // set signed client identifier
        map.put("clientIdentifierJwt", signedClientIdentifier);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<Map<String, String>>(map, headers);

        // use spring-web RestTemplate to call REST api
        RestTemplate restTemplate = new RestTemplate();

        // call Knox api to receive api access token
        ResponseEntity<MyResponse> responseEntity = restTemplate.postForEntity(URI.create(knoxGenerateApiAccessTokenEndpoint), requestEntity, MyResponse.class);

        // extract api access token
        MyResponse myResponse = responseEntity.getBody();
        apiAccessToken = myResponse.getApiAccessToken();

        return apiAccessToken;
    }
}
